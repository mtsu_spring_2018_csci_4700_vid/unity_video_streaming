﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.EventSystems;

public class Controller : MonoBehaviour
{
    private Pages pages;
    private Text headerTxt;
    private int currPage;
    private VideoPlayer videoplayer;
    private AudioSource audiosource;
    private Text playButton;
    private Canvas screenCanvas;

    private int forwardSpeed = 1;

    private Text forwardText;

    private Slider videoSlider;

    public bool changeTimeLine = false;

    /*!
     *
     *
     */
    void Start ()
    {
        pages = CreateDemoPages.Instantiate();
        headerTxt = GameObject.Find("Canvas/Header Text").GetComponent<Text>();
        videoplayer = GameObject.Find("Canvas/VideoCanvas/Video Target").GetComponent<VideoPlayer>();
        audiosource = GameObject.Find ("Canvas/VideoCanvas/Video Target").GetComponent<AudioSource> ();
        playButton = GameObject.Find ("Canvas/VideoCanvas/PlayButton").GetComponentInChildren<Text> ();
        screenCanvas = GameObject.Find ("Canvas/VideoCanvas").GetComponent<Canvas> ();
        //VideoTimeline
        videoSlider = GameObject.Find("Canvas/VideoCanvas/VideoSlider").GetComponent<Slider>();

        //Display current forward/backward speed.
        forwardText = GameObject.Find ("Canvas/Forward Text").GetComponent<Text> ();

        currPage = 0;
        StartCoroutine (DrawPage ());
    }

    public void NextPage()
    {
        forwardSpeed = 1;
        forwardText.text = "  ";
        if (currPage < pages.Count - 1)
            currPage++;
        StartCoroutine (DrawPage ());
    }

    public void PrevPage()
    {
        forwardSpeed = 1;
        forwardText.text = "  ";
        if (currPage > 0)
            currPage--;
        StartCoroutine (DrawPage ());
    }

    // Have to use Coroutine so the video can be prepare
    // Only if the video is prepared we can get some value associate with the
    // video like total frame number, frame rate => important to control the timeline.
    IEnumerator DrawPage()
    {
        //get current PageContent object
        PageContent content = pages.AtIndex(currPage);

        //update header text
        headerTxt.text = content.Header;

        //update video URL
        videoplayer.playOnAwake = false;
        audiosource.playOnAwake = false;

        videoplayer.playbackSpeed = 1;
        videoplayer.source = VideoSource.Url;
        videoplayer.url = content.VideoURL;

        // Some weird code to enable audio, No idea why the audio does not work 
        // within the videoplayer component itself.
        videoplayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        videoplayer.controlledAudioTrackCount = 1;
        videoplayer.EnableAudioTrack (0, true);
        videoplayer.SetTargetAudioSource (0, audiosource);

        // Prepared the video, only wait 5 second => avoid infinite loop.
        videoplayer.Prepare ();
        WaitForSeconds waitTime = new WaitForSeconds (5);
        while (!videoplayer.isPrepared) {
            Debug.Log ("Preparing Video");
            yield return waitTime;
            break;
        }

        Debug.Log ("Done Prepraing video");

        // Set the maximum value of the play head to the total number of frame in the video.
        // This will allow user to change the current frame in the slider.
        videoSlider.maxValue = videoplayer.frameCount;

        // If the video is reached end. Come back to the beginning, and restart the sence.
        videoplayer.loopPointReached += EndReached;

        videoplayer.Play ();
        audiosource.Play ();
    }

    // Reached the end
    void EndReached (UnityEngine.Video.VideoPlayer vp){
        vp.frame = 0;
        playButton.text = "Play";
        forwardSpeed = 1;
        videoplayer.playbackSpeed = forwardSpeed;
        audiosource.mute = false;
        forwardText.text = "   ";
    }

    // Activate when play/pause button is click
    public void pausePlayButton(){

        if (playButton.text == "Pause") {
            videoplayer.Pause ();
            audiosource.Pause ();
            playButton.text = "Play";
        } 
        else {
            videoplayer.Play ();
            audiosource.Play ();
            playButton.text = "Pause";
        }
    }

    // Full Screen Button
    public void fullScreen(){
        Debug.Log("Whattt");
    }

    // Change playback spped
    public void forwardButton(){
        if (forwardSpeed < 8) {
            forwardSpeed = forwardSpeed * 2;
            videoplayer.playbackSpeed = forwardSpeed;
            forwardText.text = "x" + forwardSpeed.ToString () + " speed";
            audiosource.mute = true;
        } 
        else {
            forwardSpeed = 1;
            videoplayer.playbackSpeed = forwardSpeed;
            forwardText.text = "     ";
            audiosource.mute = false;
        }
        Debug.Log ("Current play back speed: " + forwardSpeed.ToString ());
    }

    // Change volume
    public void VolumeSlider(float newVolumeValue)
    {
        audiosource.volume = newVolumeValue;
    }

    // Update the slider in realtime.
    private void Update(){
        if (changeTimeLine == false) {
            videoSlider.value = videoplayer.frame;
        }
    }
        
    public void changeFrame(float newFrame){
        if (changeTimeLine == true) {
            videoplayer.frame = Mathf.RoundToInt (newFrame);
            videoSlider.value = newFrame;
        }
    }
        
}
