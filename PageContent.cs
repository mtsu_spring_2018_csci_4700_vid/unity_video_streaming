﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pages
{
    /* Member Data */
    private List<PageContent> m_pageList;

    /**
     * Instance constructor
     */
    public Pages()
    {
        m_pageList = new List<PageContent>();
    }

    /**
     * This method returns the PageContent object at the specified index.
     */
    public PageContent AtIndex(int _index_)
    {
        return m_pageList[_index_];
    }

    /**
     * This method inserts a new PageContent object into the list of PageContent objects.
     */
    public void Insert(PageContent _pageContent_)
    {
        m_pageList.Add(_pageContent_);
    }

    /**
     * Accessors
     */
    public int Count
    {
        get { return m_pageList.Count; }
    }
}

public class PageContent
{
    /* Member Data */
    private string m_header;
    private string m_videoURL;

    /**
     * Instance constructor
     */
    public PageContent(string _header_, string _videoURL_)
    {
        m_header = _header_;
        m_videoURL = _videoURL_;
    }

    /**
     * Accessors.
     */
    public string Header
    {
        get { return m_header; }
    }
    public string VideoURL
    {
        get { return m_videoURL; }
    }
}

public static class CreateDemoPages
{
    /**
     * This method returns a hard-coded Pages object for demonstration purposes
     */
    public static Pages Instantiate()
    {
        int NUM_PAGES = 9;
        Pages pages = new Pages();

        for (int i = 0; i < NUM_PAGES; i++)
        {
            string header = "Item " + (i + 1).ToString();
            string URL = "http://research.vuse.vanderbilt.edu/rasl/wp-content/uploads/ASVids/" + ((i % 2)+1).ToString() + ".mp4";
            pages.Insert(new PageContent(header, URL));
        }

        return pages;
    }
}